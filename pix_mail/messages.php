<?php
	$lang = array(
        // English is the default language, you can change the langugae from the config.php file
        'EN'=> array(
            'check_email'=>'Please enter a valid email!',
            'captcha'=>'Please check the Captcha!',
            'php_error'=>'Ошибка!',
            'success'=>'Спасибо за отправку, мы вам перезвоним!',
            'subscription'=>'Thank you for your Subscription.',
            'email_exists'=>'Email Already Exists',
        )
    );
?>